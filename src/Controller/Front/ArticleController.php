<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 15:21
 */

namespace App\Controller\Front;


use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use App\Utils\Pagination;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/** @Route("/cidt")*/
class ArticleController extends AbstractController {
    /** @Route("/rechercher", name="front_article_recherche") */
    public function rechercher(Request $request, ArticleRepository $articleRepository, Pagination $pagination): Response
    {
        $page = intval($request->get('page', 1));
        $q = $request->get('q');
        $articles = $articleRepository->frontSearch($q, ($page - 1), Pagination::PER_PAGE);
        $pagination = $pagination->getPagination('front_article_recherche', $articleRepository->countFrontSearch($q), $page);
        return $this->render('front/articles.twig', ['articles' => $articles, 'pagination' => $pagination, 'q' => $q]);
    }

    /** @Route("/{cat_slug}/dossiers",name="front_articles") */
    public function articles(Request $request, string $cat_slug, CategoryRepository $categoryRepository, ArticleRepository $articleRepository, Pagination $pagination)
    {
        $page = $request->get('page', 1);
        $category = $categoryRepository->findOneBy(['slug' => $cat_slug]);
        if ($category != null) {
            $articles = $articleRepository->findBy(['published' => 1, 'category' => $category], ['createdAt' => "DESC"], Pagination::PER_PAGE, ($page - 1));
            $pagination = $pagination->getPagination('front_articles', $articleRepository->count(['published' => 1, 'category' => $category]), $page, ['cat_slug' => $cat_slug]);
            return $this->render('front/articles.twig', ['articles' => $articles, 'pagination' => $pagination, 'category' => $category]);
        } else return $this->redirectToRoute('front_home');
    }

    /** @Route("/{cat_slug}/{slug}",name="front_article") */
    public function article(string $cat_slug, string $slug, ArticleRepository $articleRepository)
    {
        $article = $articleRepository->findOneBy(['slug' => $slug]);
        if ($article !== null) {
            $article->setVue($article->getVue() + 1);
            $articleRepository->save($article);
            $articles = $articleRepository->filter(['published' => 1], null, 0, 3);
            return $this->render('front/article.twig', ['article' => $article, 'articles' => $articles]);
        } else return $this->redirectToRoute('front_home');
    }
}