<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 15:21
 */

namespace App\Controller\Front;

use App\Entity\Member;
use App\Repository\MemberRepository;
use App\Repository\PageRepository;
use App\Utils\EmailManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class MemberController extends AbstractController {

    /** @Route("/cidt/s-engager",name="front_register") */
    public function register(Request $request, MemberRepository $memberRepository, ValidatorInterface $validator,  EmailManager $emailManager,PageRepository $pageRepository): Response
    {
        if ($request->isMethod('post')) {
            $member = new Member();
            $member->setName($request->get('name'))->setEmail($request->get('email'))
                ->setPhone($request->get('phone'));
            $member->setAge($request->get('age'))->setCountry($request->get('country'))->setAdresse($request->get('adresse'));

            $errors = $validator->validate($member);
            if (count($validator->validate($member)))
                return $this->json(['type' => 'error', "text" => (string)$errors]);
            else {
                $memberRepository->save($member);

                $message = "<p>Votre adhésion à" . $this->getParameter("app_title") . " a été enregistrée </p>";
                $message .= "<p>Merci de votre confiance</p>";
                $renderEmail = $this->renderView('email/contact.twig', ['message' => $message]);
                $emailManager->sendMail($this->getParameter('no_reply'), $member->getEmail(), "Votre adhésion à " . $this->getParameter('app_title'), $renderEmail);

                $message = "<p>Nouvelle demande d'adhésion à " . $this->getParameter("app_title") . " </p>";
                $renderEmail = $this->renderView('email/contact.twig', ['message' => $message]);
                $emailManager->sendMail($this->getParameter('no_reply'),$this->getParameter('app_email'), "Nouvelle Adhésion " , $renderEmail);

                return $this->json(['type' => 'success', "text" => "Vous avez bien été enregistré"]);
            }
        }
        $member = new Member();
        $page=$pageRepository->findOneBy(['uid'=>'5d7f9e5453f32']);
        return $this->render('front/register.twig', [ 'member' => $member,'page'=>$page]);
    }

}