<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 15:21
 */

namespace App\Controller\Front;


use App\Repository\ArticleRepository;
use App\Repository\EventRepository;
use App\Utils\Pagination;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/** @Route("/cidt") */
class EventController extends AbstractController {
    /** @Route("/rechercher", name="front_event_recherche") */
    public function rechercher(Request $request, EventRepository $eventRepository, ArticleRepository $articleRepository, Pagination $pagination): Response
    {
        $page = intval($request->get('page', 1));
        $q = $request->get('q');
        $events = $eventRepository->frontSearch($q, ($page - 1), Pagination::PER_PAGE);
        $articles = $articleRepository->findBy(['published' => 1, 'category' => 3], ['createdAt' => "DESC"], 3);
        $pagination = $pagination->getPagination('front_event_recherche', $eventRepository->countFrontSearch($q), $page);
        return $this->render('front/events.twig', ['events' => $events, 'pagination' => $pagination, 'q' => $q, 'articles' => $articles]);
    }

    /** @Route("/evenements",name="front_events") */
    public function events(Request $request, EventRepository $eventRepository, ArticleRepository $articleRepository, Pagination $pagination)
    {
        $page = $request->get('page', 1);
        $events = $eventRepository->findBy(['published' => 1], ['createdAt' => "DESC"], Pagination::PER_PAGE, ($page - 1));
        $articles = $articleRepository->findBy(['published' => 1, 'category' => 3], ['createdAt' => "DESC"], 3);
        $pagination = $pagination->getPagination('front_events', $eventRepository->count(['published' => 1]), $page, []);
        return $this->render('front/events.twig', ['events' => $events, 'pagination' => $pagination, 'articles' => $articles]);
    }

    /** @Route("/evenements/{slug}/cidt",name="front_event") */
    public function event(string $slug, ArticleRepository $articleRepository, EventRepository $eventRepository)
    {
        $event = $eventRepository->findOneBy(['slug' => $slug]);
        $articles = $articleRepository->findBy(['published' => 1, 'category' => 3], ['createdAt' => "DESC"], 3);
        if ($event !== null) {
            $event->setVue($event->getVue() + 1);
            $eventRepository->save($event);
            $events = $eventRepository->filter(['published' => 1], null, 0, 3);
            return $this->render('front/event.twig', ['event' => $event, 'events' => $events, 'articles' => $articles]);
        } else return $this->redirectToRoute('front_home');
    }

}