<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 14:29
 */


namespace App\Controller\Security;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\LoginFormType;
use Symfony\Component\{HttpFoundation\Request, HttpFoundation\Response, Routing\Annotation\Route, Security\Http\Authentication\AuthenticationUtils};
use App\Repository\UserRepository;
use App\Utils\Str;
use App\Entity\User;


class SecurityController extends AbstractController
{
    /**
     * @Route("/login",name="login")
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->createForm(LoginFormType::class, ['_username' => $lastUsername]);
        $title = "Connexion à mon compte";
        $success = $request->query->get('success', '');// si redirection venant de la modification de mot de passe

        return $this->render('admin/auth/login.twig', ['form' => $form->createView(), 'error' => $error, 'title' => $title, 'success' => $success]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        throw new \Exception('this should not be reached!');
    }
}
