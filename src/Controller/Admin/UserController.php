<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin;


use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Utils\EmailManager;
use App\Utils\FileUploader;
use App\Utils\Pagination;
use App\Utils\Str;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class UserController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/users")
 */
class UserController extends AbstractController {

    /** @Route("/profils/{uid}",name="admin_profils") */
    public function profils(RouterInterface $router, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user, ["action" => $router->generate("admin_edit_profil", ['uid' => $user->getUid()])]);
        return $this->render("admin/users/profil.twig", ["form" => $form->createView(), 'user' => $user]);
    }

    /** @Route("",name="admin_users") */
    public function index(Request $request, UserRepository $userRepository, Pagination $pagination): Response
    {
        $page = $request->get('page', 1);
        $action = $request->query->get('doaction', '');
        if ($action == "Appliquer") {
            $todo = intval($request->get('todo'));
            if ($todo == -1) $userRepository->removeBy(['uid' => $request->get('uid')]);
            return $this->redirectToRoute('admin_users');
        } elseif ($action == "Filtrer") {
            $data = ["doaction" => "Filtrer"];
            $email = $request->get('email');
            if (trim($email) !== "") $data['email'] = $email;
            $users = $userRepository->filter($data, ($page - 1), Pagination::PER_PAGE);
            $pagination = $pagination->getPagination('admin_users', $userRepository->count([]), $page, $data);
            return $this->render("admin/users/users.twig", ['users' => $users, 'pagination' => $pagination]);
        } else {
            $users = $userRepository->last($this->getUser(), ($page - 1), Pagination::PER_PAGE);
            $pagination = $pagination->getPagination('admin_articles', $userRepository->count([]), $page);
            return $this->render("admin/users/users.twig", ['users' => $users, 'pagination' => $pagination]);
        }

    }

    /** @Route("/add",name="admin_add_user") */
    public function add(Request $request, UserRepository $repository, EmailManager $emailManager, Str $str)
    {
        $user = new User();
        $plainPassword = $str->str_random(12);
        $user->setPlainPassword($plainPassword)->setPublished(true);
        $form = $this->createForm(UserType::class, $user);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $user->setRoles([$request->get('role')])->setPassword($str->encodePassword($user))->setPhoto("no-image.png");
                $repository->save($user);
                $message = "<p>Votre compte " . $this->getParameter("app_title") . " a été créé </p>";
                $message .= "<p>Identifiant de connection</p>";
                $message .= "<p>Email: <em>" . $user->getEmail() . "</em></p>";
                $message .= "<p>Mot de passe: <em>" . $plainPassword . "</em></p>";
                $renderEmail = $this->renderView('email/contact.twig', ['message' => $message]);
                $emailManager->sendMail($this->getParameter('no_reply'), $user->getEmail(), "Création de compte : message de confirmation", $renderEmail);
                $this->addFlash('success', "Utilisateur ajouté avec succès");
                return $this->redirectToRoute('admin_users');
            } else
                $this->addFlash('error', (string)$form->getErrors(true));
        }
        return $this->render("admin/users/add_user.twig", ['form' => $form->createView()]);
    }

    /** @Route("/edit/password", name="admin_edit_password") */
    public function editPassword(Request $request, UserRepository $repository, Str $str)
    {
        if ($request->isMethod('POST')) {
            $password = $request->get('password');
            $password_confirm = $request->get('password_confirm');
            if ($password === $password_confirm) {
                $user = $this->getUser();
                $user->setPlainPassword($password)->setPassword($str->encodePassword($user));
                $repository->save($user);
                $this->addFlash('success', "Mot de passe modifé.");
            } else $this->addFlash('error', "La confirmation du mot de passe ne correspond pas au mot de passe.");
        }
        return $this->redirectToRoute('admin_profils', ['uid' => $this->getUser()->getUid()]);
    }

    /** @Route("/edit/{uid}",name="admin_edit_profil") */
    public function edit(Request $request, User $user, UserRepository $userRepository, RouterInterface $router, FileUploader $uploader): Response
    {
        $form = $this->createForm(UserType::class, $user, ["action" => $router->generate("admin_edit_profil", ['uid' => $user->getUid()])]);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($user->getPhoto()) {
                    $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('photo')->getData(), 'users');
                    if ($response['uploaded'])
                        $user->setPhoto($response['fileName']);
                } else
                    $user->setPhoto($request->get('previous_photo'));
                $user->setRoles([$request->get('role')]);
                $userRepository->save($user);
                $this->addFlash('success', "Profils modifié avec succès");
                return $this->redirectToRoute('admin_profils', ['uid' => $user->getUid()]);
            } else {
                $form->get('photo')->setData($user->getPhoto());
                $this->addFlash('error', $form->getErrors());
            }
        }
        return $this->render("admin/users/profil.twig", ["form" => $form->createView(), 'user' => $user]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_user")
     */
    public function delete(User $user, UserRepository $userRepository): Response
    {
        if (!$user->hasRole('ROLE_ADMIN')) {
            $fileName = $this->getParameter('upload_path') . "articles/" . $user->getImage();
            if (file_exists($fileName))
                unlink($fileName);
            $userRepository->remove($user);
            $this->addFlash('success', "Utilisateur supprimé avec succès.");
        } else  $this->addFlash('error', "Impossible de supprimer cet utilisateur.(Role ADMIN)");

        return $this->redirectToRoute('admin_users');
    }
}
