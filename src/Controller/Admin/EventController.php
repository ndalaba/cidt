<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin;


use App\Entity\Event;
use App\Form\EventType;
use App\Repository\PersonRepository;
use App\Repository\EventRepository;
use App\Utils\FileUploader;
use App\Utils\Pagination;
use App\Utils\Str;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class EventController
 * @package App\Controller\Admin
 * @Route("/admin/events")
 */
class EventController extends AbstractController {
    /** @Route("",name="admin_events") */
    public function index(Request $request, EventRepository $eventRepository, Pagination $pagination): Response {
        $page = $request->get('page', 1);
        $action = $request->query->get('doaction', '');
        if ($action == "Appliquer") {
            $todo = intval($request->get('todo'));
            if ($todo == -1) $eventRepository->removeBy(['uid' => $request->get('uid')]);
            if ($todo == 1) $eventRepository->publish($request->get('uid'));
            if ($todo == 0) $eventRepository->unpublish($request->get('uid'));
            return $this->redirectToRoute('admin_events');
        } elseif ($action == "Filtrer") {
            $data = [];
            $title = $request->get('title');
            $published = $request->get('published');
            if (trim($title) !== "") $data['title'] = $title;
            if (trim($published) !== "") $data['published'] = intval($published);
            $events = $eventRepository->filter($data, $this->getUser(), ($page - 1)*Pagination::PER_PAGE, Pagination::PER_PAGE);
            $data = ["doaction" => "Filtrer"];
            $pagination = $pagination->getPagination('admin_events', $eventRepository->count([], $this->getUser()), $page, $data);
            return $this->render("admin/events/events.twig", ['events' => $events,  'pagination' => $pagination]);
        } else {
            $events = $eventRepository->last($this->getUser(), ($page - 1)*Pagination::PER_PAGE, Pagination::PER_PAGE);
            $pagination = $pagination->getPagination('admin_events', $eventRepository->count([], $this->getUser()), $page);
       
            return $this->render("admin/events/events.twig", ['events' => $events, 'pagination' => $pagination]);
        }
    }

    /** @Route("/add",name="admin_add_event",methods={"POST","GET"}) */
    public function add(Request $request, EventRepository $repository, FileUploader $uploader, RouterInterface $router,Str $str) {
        $event = new Event();
        $form = $this->createForm(EventType::class, $event, ["action" => $router->generate("admin_add_event")]);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('image')->getData(), 'events');
                if ($response['uploaded'])
                    $event->setImage($response['fileName']);
               $event->setUser($this->getUser());
                $event->setSlug($str->slug($event->getTitle()));
                $repository->save($event);
                $this->addFlash('success', "Evènement ajouté avec succès");
                return $this->redirectToRoute('admin_events');
            } else
                $this->addFlash('error', $form->getErrors());
        }
        return $this->render('admin/events/form.twig', ['form' => $form->createView()]);
    }

    /** @Route("/edit/{uid}",name="admin_edit_event") */
    public function edit(Request $request, Event $event, EventRepository $eventRepository, RouterInterface $router, FileUploader $uploader, Str $str, PersonRepository $personRepository): Response {
        $form = $this->createForm(EventType::class, $event, ["action" => $router->generate("admin_edit_event", ['uid' => $request->get('uid')])]);
        $authors = $personRepository->findAll();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->isValid()) {
                if ($event->getImage()) {
                    $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('image')->getData(), 'events');
                    if ($response['uploaded'])
                        $event->setImage($response['fileName']);
                } else
                    $event->setImage($request->get('previous_image'));
                $event->setSlug($str->slug($event->getTitle()));
                $eventRepository->save($event);
                $this->addFlash('success', "Evènement modifié avec succès");
                return $this->redirectToRoute('admin_events');
            } else
                $this->addFlash('error', $form->getErrors());
        }

        return $this->render('admin/events/form.twig', ['form' => $form->createView(), 'event' => $event]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_event")
     */
    public function delete(Event $event, EventRepository $eventRepository): Response {
        $fileName = $this->getParameter('upload_path') . "events/" . $event->getImage();
        if (file_exists($fileName))
            unlink($fileName);
        $eventRepository->remove($event);
        $this->addFlash('success', "Evènement supprimé avec succès");
        return $this->redirectToRoute('admin_events');
    }
}