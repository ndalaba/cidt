<?php

/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 1:06 PM
 */

namespace App\Controller\Admin;


use App\Repository\ArticleRepository;
use App\Repository\MemberRepository;
use App\Repository\PageRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class IndexController
 * @package App\Controller\Admin
 * @Route("/admin")
 */
class IndexController extends AbstractController {

    /** @Route("/index",name="admin_home") */
    public function index(ArticleRepository $articleRepository,   PageRepository $pageRepository, UserRepository $userRepository,MemberRepository $memberRepository)
    {
        $articles_published = $articleRepository->count(['published' => true,'category'=>3]);
        $projets_published = $articleRepository->count(['published' => true,'category'=>1]);
        $page_activated = $pageRepository->count(['published' => true]);
        $users_activated = $memberRepository->count(['validated' => true]);
        $output = [ 'articles' => $articles_published,  'users_activated' => $users_activated, 'pages' => $page_activated, 'projets'=>$projets_published];

        return $this->render('admin/home.twig', $output);
    }
}
