<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin;


use App\Entity\Person;
use App\Form\PersonType;
use App\Repository\PersonRepository;
use App\Utils\FileUploader;
use App\Utils\Pagination;
use App\Utils\Str;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class PersonController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/persons")
 */
class PersonController extends AbstractController {
    /** @Route("",name="admin_persons") */
    public function index(Request $request, PersonRepository $personRepository, RouterInterface $router, Pagination $pagination): Response {
        $form = $this->createForm(PersonType::class, new Person(), ["action" => $router->generate("admin_add_person")]);
        $page = $request->get('page', 1);
        $persons = $personRepository->last($this->getUser(),($page - 1));
        $pagination = $pagination->getPagination('admin_persons', $personRepository->count([]), $page);
        return $this->render("admin/persons/persons.twig", ["form" => $form->createView(), 'persons' => $persons, 'pagination' => $pagination]);
    }

    /** @Route("/add",name="admin_add_person",methods={"POST"}) */
    public function add(Request $request, PersonRepository $repository, Str $str, FileUploader $uploader) {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $person->setSlug($str->slug($person->getName()));
            $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('image')->getData(), 'persons');
            if ($response['uploaded'])
                $person->setImage($response['fileName']);
            $repository->save($person);
            $this->addFlash('success', "Personne ajoutée avec succès");
        } else
            $this->addFlash('error', "Erreur formulaire");
        return $this->redirectToRoute('admin_persons');
    }

    /** @Route("/edit/{uid}",name="admin_edit_person") */
    public function edit(Request $request, Person $person, PersonRepository $personRepository, RouterInterface $router, Pagination $pagination, Str $str, FileUploader $uploader): Response {
        $form = $this->createForm(PersonType::class, $person, ["action" => $router->generate("admin_edit_person", ['uid' => $person->getUid()])]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $person->setSlug($str->slug($person->getName()));
            if ($person->getImage()) {
                $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('image')->getData(), 'persons');
                if ($response['uploaded'])
                    $person->setImage($response['fileName']);
            } else
                $person->setImage($request->get('previous_image'));
            $personRepository->save($person);
            $this->addFlash('success', "Personne modifiée avec succès");
            return $this->redirectToRoute('admin_persons');
        }
        $page = $request->get('page', 1);
        $persons = $personRepository->last($this->getUser(),($page - 1));
        $pagination = $pagination->getPagination('admin_persons', $personRepository->count([]), $page);
        return $this->render('admin/persons/persons.twig', ['form' => $form->createView(), 'persons' => $persons, 'person' => $person, 'pagination' => $pagination]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_person")
     */
    public function delete(Person $person, PersonRepository $personRepository): Response {
        $fileName = $this->getParameter('upload_path') . "articles/" . $person->getImage();
        if (file_exists($fileName))
            unlink($fileName);
        $personRepository->remove($person);
        $this->addFlash('success', "Personne supprimée avec succès");
        return $this->redirectToRoute('admin_persons');
    }
}