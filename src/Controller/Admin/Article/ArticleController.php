<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin\Article;


use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use App\Utils\FileUploader;
use App\Utils\Pagination;
use App\Utils\Str;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ArticleController
 * @package App\Controller\Admin
 * @Route("/admin/articles")
 */
class ArticleController extends AbstractController {
    /** @Route("",name="admin_articles") */
    public function index(Request $request, ArticleRepository $articleRepository, CategoryRepository $categoryRepository, Pagination $pagination): Response
    {
        $page = $request->get('page', 1);
        $action = $request->query->get('doaction', '');
        if ($action == "Appliquer") {
            $todo = intval($request->get('todo'));
            if ($todo == -1) $articleRepository->removeBy(['uid' => $request->get('uid')]);
            if ($todo == 1) $articleRepository->publish($request->get('uid'));
            if ($todo == 0) $articleRepository->unpublish($request->get('uid'));
            if ($todo == 2) $articleRepository->setTop($request->get('uid'));
            if ($todo == -2) $articleRepository->setNormal($request->get('uid'));
            return $this->redirectToRoute('admin_articles');
        } elseif ($action == "Filtrer") {
            $data = [];
            $title = $request->get('title');
            $top = $request->get('top');
            $published = $request->get('published');
            $category_id = $request->get('category');
            if (trim($title) !== "") $data['title'] = $title;
            if (trim($top) !== "") $data['top'] = intval($top);
            if (trim($published) !== "") $data['published'] = intval($published);
            if (trim($category_id) !== "") $data['category'] = intval($category_id);
            $articles = $articleRepository->filter($data, $this->getUser(), ($page - 1), Pagination::PER_PAGE);
            $data = ["doaction" => "Filtrer"];
            $pagination = $pagination->getPagination('admin_articles', $articleRepository->count([], $this->getUser()), $page, $data);
            $categories = $categoryRepository->findAll();
            return $this->render("admin/articles/articles.twig", ['articles' => $articles, 'categories' => $categories, 'pagination' => $pagination]);
        } else {
            $articles = $articleRepository->last($this->getUser(), ($page - 1), Pagination::PER_PAGE);
            $pagination = $pagination->getPagination('admin_articles', $articleRepository->count([], $this->getUser()), $page);
            $categories = $categoryRepository->findAll();
            return $this->render("admin/articles/articles.twig", ['articles' => $articles, 'categories' => $categories, 'pagination' => $pagination]);
        }
    }

    /** @Route("/add",name="admin_add_article",methods={"POST","GET"}) */
    public function add(Request $request, ArticleRepository $repository, FileUploader $uploader, RouterInterface $router, UserRepository $userRepository, Str $str)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article, ["action" => $router->generate("admin_add_article")]);
        $authors = $userRepository->findAll();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('image')->getData(), 'articles', $article->getUid());
                if ($response['uploaded'])
                    $article->setImage($response['fileName']);
                $this->getUser()->hasRole('ROLE_ADMIN') ? $article->setUser($userRepository->find($request->get('user_id'))) : $article->setUser($this->getUser());
                $article->setSlug($str->slug($article->getTitle()));
                $article->setContent($str->removeFont($article->getContent()));
                $repository->save($article);
                $this->addFlash('success', "Article ajouté avec succès");
                return $this->redirectToRoute('admin_articles');
            } else
                $this->addFlash('error', $form->getErrors());
        }
        return $this->render('admin/articles/form.twig', ['form' => $form->createView(), 'authors' => $authors]);
    }

    /** @Route("/edit/{uid}",name="admin_edit_article") */
    public function edit(Request $request, Article $article, ArticleRepository $articleRepository, RouterInterface $router, FileUploader $uploader, Str $str, UserRepository $userRepository): Response
    {
        $form = $this->createForm(ArticleType::class, $article, ["action" => $router->generate("admin_edit_article", ['uid' => $request->get('uid')])]);
        $authors = $userRepository->findAll();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->isValid()) {
                if ($article->getImage()) {
                    $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('image')->getData(), 'articles', $article->getUid());
                    if ($response['uploaded'])
                        $article->setImage($response['fileName']);
                } else
                    $article->setImage($request->get('previous_image'));
                $article->setSlug($str->slug($article->getTitle()));
                $article->setContent($str->removeFont($article->getContent()));
                $articleRepository->save($article);
                $this->addFlash('success', "Article modifié avec succès");
                return $this->redirectToRoute('admin_articles');
            } else
                $this->addFlash('error', $form->getErrors());
        }

        return $this->render('admin/articles/form.twig', ['form' => $form->createView(), 'article' => $article, 'authors' => $authors]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_article")
     */
    public function delete(Article $article, ArticleRepository $articleRepository): Response
    {
        $fileName = $this->getParameter('upload_path') . "articles/" . $article->getImage();
        if (file_exists($fileName))
            unlink($fileName);
        $articleRepository->remove($article);
        $this->addFlash('success', "Article supprimé avec succès");
        return $this->redirectToRoute('admin_articles');
    }
}