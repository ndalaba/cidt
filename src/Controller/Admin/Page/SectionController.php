<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin\Page;


use App\Entity\Section;
use App\Form\SectionType;
use App\Repository\SectionRepository;
use App\Utils\Str;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SectionController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/pages/sections")
 */
class SectionController extends AbstractController {
    /** @Route("",name="admin_sections") */
    public function index(SectionRepository $sectionRepository, RouterInterface $router): Response {
        $form = $this->createForm(SectionType::class, new Section(), ["action" => $router->generate("admin_add_section")]);
        $sections = $sectionRepository->findAll();
        return $this->render("admin/pages/sections/sections.twig", ["form" => $form->createView(), 'sections' => $sections]);
    }

    /** @Route("/add",name="admin_add_section",methods={"POST"}) */
    public function add(Request $request, SectionRepository $repository, Str $str) {
        $section = new Section();
        $form = $this->createForm(SectionType::class, $section);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $section->setSlug($str->slug($section->getTitle()));
            $repository->save($section);
            $this->addFlash('success', "Section ajoutée avec succès");
        } else
            $this->addFlash('error', "Erreur formulaire");
        return $this->redirectToRoute('admin_sections');
    }

    /** @Route("/edit/{uid}",name="admin_edit_section") */
    public function edit(Request $request, Section $section, SectionRepository $sectionRepository, RouterInterface $router, Str $str): Response {
        $form = $this->createForm(SectionType::class, $section, ["action" => $router->generate("admin_edit_section",['uid'=>$section->getUid()])]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $section->setSlug($str->slug($section->getTitle()));
            $sectionRepository->save($section);
            $this->addFlash('success', "Section modifiée avec succès");
            return $this->redirectToRoute('admin_sections');
        }
        $sections = $sectionRepository->findAll();
        return $this->render("admin/pages/sections/sections.twig", ["form" => $form->createView(), 'sections' => $sections]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_section")
     */
    public function delete(Section $section, SectionRepository $sectionRepository): Response {
        $sectionRepository->remove($section);
        $this->addFlash('success', "Section supprimée avec succès");
        return $this->redirectToRoute('admin_sections');
    }
}