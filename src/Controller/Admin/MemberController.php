<?php
/**
 * Created by PhpStorm.
 * Member: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin;


use App\Entity\Member;
use App\Form\MemberType;
use App\Repository\MemberRepository;
use App\Utils\Pagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class MemberController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/members")
 */
class MemberController extends AbstractController {

    /** @Route("/{uid}",name="admin_member_profils") */
    public function profils(RouterInterface $router, Member $member): Response
    {
        //$form = $this->createForm(MemberType::class, $member, ["action" => $router->generate("admin_edit_member", ['uid' => $member->getUid()])]);
        return $this->render("admin/members/profil.twig", ['member' => $member]);
    }

    /** @Route("",name="admin_members") */
    public function index(Request $request, MemberRepository $memberRepository, Pagination $pagination): Response
    {
        $page = $request->get('page', 1);
        $action = $request->query->get('doaction', '');
        if ($action == "Appliquer") {
            $todo = intval($request->get('todo'));
            if ($todo == -1) $memberRepository->removeBy(['uid' => $request->get('uid')]);
            if ($todo == 1) $memberRepository->activate($request->get('uid'));
            if ($todo == 0) $memberRepository->deActivate($request->get('uid'));
            return $this->redirectToRoute('admin_members');
        } elseif ($action == "Filtrer") {
            $data = ["doaction" => "Filtrer"];
            $email = $request->get('email');
            $activated = $request->get('active');
            if (trim($email) !== "") $data['email'] = $email;
            if (trim($activated) !== "") $data['activated'] = intval($activated);
            $members = $memberRepository->filter($data, ($page - 1), Pagination::PER_PAGE);
            $pagination = $pagination->getPagination('admin_members', $memberRepository->count([]), $page, $data);
            return $this->render("admin/members/members.twig", ['members' => $members, 'pagination' => $pagination]);
        } else {
            $members = $memberRepository->last($this->getUser(), ($page - 1), Pagination::PER_PAGE);
            $pagination = $pagination->getPagination('admin_members', $memberRepository->count([]), $page);
            return $this->render("admin/members/members.twig", ['members' => $members, 'pagination' => $pagination]);
        }

    }

    /** @Route("/edit/{uid}",name="admin_edit_member") */
    public function edit(Request $request, Member $member, MemberRepository $memberRepository, RouterInterface $router): Response
    {
        $form = $this->createForm(MemberType::class, $member, ["action" => $router->generate("admin_edit_profil", ['uid' => $member->getUid()])]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $memberRepository->save($member);
            $this->addFlash('success', "Membre modifié avec succès");
            return $this->redirectToRoute('admin_profils', ['uid' => $member->getUid()]);
            } else {
                $this->addFlash('error', $form->getErrors());
            }
        return $this->render("admin/members/profil.twig", ["form" => $form->createView(),'member'=>$member]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_member")
     */
    public function delete(Member $member, MemberRepository $memberRepository): Response
    {
        $memberRepository->remove($member);
        $this->addFlash('success', "Membre supprimé avec succès.");
        return $this->redirectToRoute('admin_members');
    }
}