<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin;


use App\Entity\Partenaire;
use App\Form\PartenaireType;
use App\Repository\PartenaireRepository;
use App\Utils\FileUploader;
use App\Utils\Pagination;
use App\Utils\Str;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class PartenaireController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/partenaires")
 */
class PartenaireController extends AbstractController {
    /** @Route("",name="admin_partenaires") */
    public function index(Request $request, PartenaireRepository $partenaireRepository, RouterInterface $router, Pagination $pagination): Response {
        $form = $this->createForm(PartenaireType::class, new Partenaire(), ["action" => $router->generate("admin_add_partenaire")]);
        $page = $request->get('page', 1);
        $partenaires = $partenaireRepository->last($this->getUser(),($page - 1));
        $pagination = $pagination->getPagination('admin_partenaires', $partenaireRepository->count([]), $page);
        return $this->render("admin/partenaires/partenaires.twig", ["form" => $form->createView(), 'partenaires' => $partenaires, 'pagination' => $pagination]);
    }

    /** @Route("/add",name="admin_add_partenaire",methods={"POST"}) */
    public function add(Request $request, PartenaireRepository $repository, Str $str, FileUploader $uploader) {
        $partenaire = new Partenaire();
        $form = $this->createForm(PartenaireType::class, $partenaire);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $partenaire->setSlug($str->slug($partenaire->getName()));
            $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('image')->getData(), 'partenaires');
            if ($response['uploaded'])
                $partenaire->setImage($response['fileName']);
            $repository->save($partenaire);
            $this->addFlash('success', "Partenairene ajoutée avec succès");
        } else
            $this->addFlash('error', "Erreur formulaire");
        return $this->redirectToRoute('admin_partenaires');
    }

    /** @Route("/edit/{uid}",name="admin_edit_partenaire") */
    public function edit(Request $request, Partenaire $partenaire, PartenaireRepository $partenaireRepository, RouterInterface $router, Pagination $pagination, Str $str, FileUploader $uploader): Response {
        $form = $this->createForm(PartenaireType::class, $partenaire, ["action" => $router->generate("admin_edit_partenaire", ['uid' => $partenaire->getUid()])]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $partenaire->setSlug($str->slug($partenaire->getName()));
            if ($partenaire->getImage()) {
                $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('image')->getData(), 'partenaires');
                if ($response['uploaded'])
                    $partenaire->setImage($response['fileName']);
            } else
                $partenaire->setImage($request->get('previous_image'));
            $partenaireRepository->save($partenaire);
            $this->addFlash('success', "Partenairene modifiée avec succès");
            return $this->redirectToRoute('admin_partenaires');
        }
        $page = $request->get('page', 1);
        $partenaires = $partenaireRepository->last($this->getUser(),($page - 1));
        $pagination = $pagination->getPagination('admin_partenaires', $partenaireRepository->count([]), $page);
        return $this->render('admin/partenaires/partenaires.twig', ['form' => $form->createView(), 'partenaires' => $partenaires, 'partenaire' => $partenaire, 'pagination' => $pagination]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_partenaire")
     */
    public function delete(Partenaire $partenaire, PartenaireRepository $partenaireRepository): Response {
        $partenaireRepository->remove($partenaire);
        $this->addFlash('success', "Partenairene supprimée avec succès");
        return $this->redirectToRoute('admin_partenaires');
    }
}