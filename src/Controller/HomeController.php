<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 15:21
 */

namespace App\Controller;


use App\Repository\ArticleRepository;
use App\Repository\EventRepository;
use App\Repository\PageRepository;
use App\Repository\PartenaireRepository;
use App\Repository\PersonRepository;
use App\Utils\EmailManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController {
    /** @Route("", name="front_home") */
    public function home(ArticleRepository $articleRepository, EventRepository $eventRepository): Response
    {
        $events = $eventRepository->findBy(['published' => 1], ['createdAt' => "DESC"], 3, 0);
        $articles = $articleRepository->findBy(['category' => 3, 'published' => 1], ['createdAt' => "DESC"], 3, 0);
        $projets = $articleRepository->findBy(['category' => 1, 'published' => 1], ['createdAt' => "DESC"], 2, 0);
        $output = ['articles' => $articles, 'projets' => $projets, 'events' => $events];
        return $this->render("front/home.twig", $output);
    }

    /** @Route("/cidt/don",name="front_don") */
    public function don()
    {
        return $this->render('front/don.twig');
    }

    /** @Route("/cidt/partenaire",name="front_partenaire") */
    public function partenaire(PartenaireRepository $partenaireRepository)
    {
        $partenaires = $partenaireRepository->findBy(['published' => true]);
        return $this->render('front/partenaires.twig', ['partenaires' => $partenaires]);
    }

    /** @Route("/cidt/contact",name="front_contact") */
    public function contact(Request $request, EmailManager $emailManager)
    {
        if ($request->isMethod('post')) {
            $email = $request->get('email');
            $name = $request->get('name');
            $message = $request->get('message');
            if (!filter_var(trim($email), FILTER_VALIDATE_EMAIL) && !empty($name)) {
                return $this->redirectToRoute("front_contact");
            }
            $renderEmail = $this->renderView('email/contact.twig', ['message' => $message]);
            $emailManager->sendMail($email, $this->getParameter("email"), ' Message de contact', $renderEmail);
            $this->addFlash('success', "Votre message a été envoyé");
        }
        return $this->render('front/contact.twig');
    }

    /** @Route("cidt/{slug}",name="front_page") */
    public function page(string $slug, PageRepository $pageRepository, PersonRepository $personRepository)
    {
        $page = $pageRepository->findOneBy(['slug' => $slug]);
        if ($page != null) {
            $persons = $personRepository->findBy(['published' => 1]);
            return $this->render('front/page.twig', ['page' => $page, 'persons' => $persons]);
        } else return $this->redirectToRoute('front_home');
    }


}