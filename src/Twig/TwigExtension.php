<?php

namespace App\Twig;

use App\Utils\Date_Time;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension {
    /**
     * @var Date_Time
     */
    private $date_time;

    public function __construct(Date_Time $date_Time)
    {
        $this->date_time = $date_Time;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('date_format_to_french', [$this, 'dateFormatToFrench']),
            new TwigFilter('truncate', [$this, 'truncate']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('function_name', [$this, 'dateFormatToFrench']),
            new TwigFunction('function_name', [$this, 'truncate']),
        ];
    }

    public function dateFormatToFrench($value, $format)
    {
        return $this->date_time->dateToShortFrench($value, $format);
    }

    public function truncate(string $value, int $size = 10)
    {
        return substr($value, 0, $size);
    }
}
