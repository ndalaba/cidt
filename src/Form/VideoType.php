<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Video;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VideoType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', TextType::class, ['required' => true])
            ->add('video', TextType::class, ['required' => true])
            ->add('description', TextareaType::class)
            ->add('published', CheckboxType::class)
            ->add('category', EntityType::class, ['class' => Category::class,'required'=>true,'placeholder' => "Catégories"]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Video::class,
            'allow_extra_fields' => true
        ]);
    }

    public function getBlockPrefix() {
        return "";
    }
}
