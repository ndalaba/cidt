<?php

namespace App\Form;

use App\Entity\Event;
use App\Entity\Partenaire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\{AbstractType,
    Extension\Core\Type\CheckboxType,
    Extension\Core\Type\DateTimeType,
    Extension\Core\Type\EmailType,
    Extension\Core\Type\FileType,
    Extension\Core\Type\TextareaType,
    Extension\Core\Type\TextType,
    FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('published', CheckboxType::class, ['label' => "Publié", 'required' => false])
            ->add('title', TextType::class, ['label' => "Titre", "attr" => ['class' => "form-control"]])
            ->add('phone', TextType::class, ['label' => "Téléphone", "attr" => ['class' => "form-control"], 'required' => false])
            ->add('email', EmailType::class, ['label' => "Email", "attr" => ['class' => "form-control"], 'required' => false])
            ->add('location', TextType::class, ['label' => "Adresse", "attr" => ['class' => "form-control"]])
            ->add('beginAt', DateTimeType::class, ['label' => "Début", "widget" => "single_text", "attr" => ['class' => "form-control"]])
            ->add('endAt', DateTimeType::class, ['label' => "Fin", "widget" => "single_text", "attr" => ['class' => "form-control"]])
            ->add('content', TextareaType::class, ['label' => "Contenu", "attr" => ['class' => "form-control summernote"]])
            ->add('image', FileType::class, ['label' => "Ajouter une image", 'data_class' => null, "attr" => ['class' => "form-control"], 'required' => false])
            ->add('partenaires', EntityType::class, ['class' => Partenaire::class, "multiple" => true, "required" => false, "attr" => ['class' => "form-control"], 'placeholder' => "Partenaire", "choice_label" => function (Partenaire $partenaire) {
                return $partenaire->getName();
            }]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            "allow_extra_fields" => true
        ]);
    }

    public function getBlockPrefix()
    {
        return "";
    }
}
