<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\{AbstractType,
    Extension\Core\Type\CheckboxType,
    Extension\Core\Type\FileType,
    Extension\Core\Type\TextareaType,
    Extension\Core\Type\TextType,
    FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('published', CheckboxType::class, ['label' => "Publié", 'required' => false])
            ->add('top', CheckboxType::class, ['label' => "A la une", 'required' => false])
            ->add('title', TextType::class, ['label' => "Titre", "attr" => ['class' => "form-control"]])
            ->add('content', TextareaType::class, ['label' => "Contenu", "attr" => ['class' => "form-control summernote"]])
            ->add('image', FileType::class, ['label' => "Ajouter une image", 'data_class' => null, "attr" => ['class' => "form-control"], 'required' => false])
            ->add('category', EntityType::class, ['class' => Category::class, "attr" => ['class' => "form-control"], 'placeholder' => "Catégories"]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Article::class,
            "allow_extra_fields"=>true
        ]);
    }

    public function getBlockPrefix() {
        return "";
    }
}
