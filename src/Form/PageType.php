<?php

namespace App\Form;

use App\Entity\Section;
use App\Entity\Page;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title')
            ->add('content')
            ->add('image', FileType::class, ['data_class' => null,'required'=>false])
            ->add('section', EntityType::class, ['class' => Section::class, "attr" => ['class' => "form-control"], 'placeholder' => "Section","required"=>false])
            ->add('published');
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Page::class,
            "allow_extra_fields" => true
        ]);
    }

    public function getBlockPrefix() {
        return "";
    }
}
