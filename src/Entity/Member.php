<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class Member
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="Members")
 */
class Member extends Entity {

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=191, nullable=false)
     * @Assert\NotBlank(message="Le champ nom ne doit pas être vide")
     */
    private $name;

    /**
     * @ORM\Column(type="string",length=191, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string",length=191, nullable=true)
     * @Assert\Email(message="N'est pas une adresse email")
     */
    private $email;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $age;

    /**
     * @var string
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private $country;

    /**
     * @var string
     * @ORM\Column(type="text", length=191, nullable=true)
     */
    private $adresse;

    /**
     * @var  bool
     * @ORM\Column(type="boolean")
     */
    private $validated = false;

    public function __construct(string $name = "")
    {
        $this->setUid()->setName($name);
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Member
     */
    public function setId(int $id): Member
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Member
     */
    public function setName(string $name): Member
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Member
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Member
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return bool
     */
    public function isValidated(): bool
    {
        return $this->validated;
    }

    /**
     * @param bool $validated
     * @return Member
     */
    public function setValidated(bool $validated): Member
    {
        $this->validated = $validated;
        return $this;
    }

    /**
     * @return int
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return Member
     */
    public function setAge(?int $age): Member
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return Member
     */
    public function setCountry(string $country): Member
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdresse(): string
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     * @return Member
     */
    public function setAdresse(string $adresse): Member
    {
        $this->adresse = $adresse;
        return $this;
    }


}