<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Event
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="Events")
 */
class Event extends Entity {

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $slug;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @var \DateTime
     * @ORM\Column(name="begin_at", type="datetime", nullable=false)
     */
    private $beginAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="end_at", type="datetime", nullable=false)
     */
    private $endAt;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $location;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $email;

    /**
     * @var Partenaire[] | ArrayCollection
     * @ORM\ManyToMany(targetEntity="Partenaire", inversedBy="events",fetch="EAGER")
     */
    private $partenaires;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="events")
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $vue=0;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     * @Assert\Image(mimeTypes = {"image/jpeg", "image/gif", "image/png"}, mimeTypesMessage="Les formats d'images acceptés sont jpg, png, gif")
     */
    private $image="noimage.png";

    public function __construct(string $title = "", string $slug = "") {
        $this->setUid()->setTitle($title)->setSlug($slug);
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Event
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Event
     */
    public function setTitle(string $title): Event {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Event
     */
    public function setSlug(string $slug): Event {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Event
     */
    public function setContent(string $content): Event {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Event
     */
    public function setImage($image): Event {
        $this->image = $image;
        return $this;
    }

    public function getExtrait(int $len = 150): ?string {
        return substr(strip_tags($this->getContent()), 0, $len);
    }

    /**
     * @return \DateTime
     */
    public function getBeginAt(): ?\DateTime
    {
        return $this->beginAt;
    }

    /**
     * @param \DateTime $beginAt
     * @return Event
     */
    public function setBeginAt(\DateTime $beginAt): Event
    {
        $this->beginAt = $beginAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndAt(): ? \DateTime
    {
        return $this->endAt;
    }

    /**
     * @param \DateTime $endAt
     * @return Event
     */
    public function setEndAt(\DateTime $endAt): Event
    {
        $this->endAt = $endAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string $location
     * @return Event
     */
    public function setLocation(string $location): Event
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Event
     */
    public function setPhone(string $phone): Event
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Event
     */
    public function setEmail(string $email): Event
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Partenaire[]|ArrayCollection
     */
    public function getPartenaires()
    {
        return $this->partenaires;
    }

    /**
     * @param Partenaire[]|ArrayCollection $partenaires
     * @return Event
     */
    public function setPartenaires($partenaires)
    {
        $this->partenaires = $partenaires;
        return $this;
    }

    /**
     * @return int
     */
    public function getVue(): int
    {
        return $this->vue;
    }

    /**
     * @param int $vue
     * @return Event
     */
    public function setVue(int $vue): Event
    {
        $this->vue = $vue;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Event
     */
    public function setUser(User $user): Event
    {
        $this->user = $user;
        return $this;
    }
}