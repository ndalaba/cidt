<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class Partenaire
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="partenaires")
 */
class Partenaire extends Entity {

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     * @Assert\NotBlank(message="Le champ nom ne doit pas être vide")
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $slug;

    /**
     * @var Event[] | ArrayCollection
     * @ORM\ManyToMany(targetEntity="Event", mappedBy="partenaires",fetch="EAGER")
     */
    private $events;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     * @Assert\Image(mimeTypes = {"image/jpeg", "image/gif", "image/png"}, mimeTypesMessage="Les formats d'images acceptés sont jpg, png, gif")
     */
    private $image = 'noimage.png';
    /**
     * @var string
     * @ORM\Column(name="url", type="string", length=191)
     */
    private $url = "#";

    public function __construct(string $name = "", string $slug = "")
    {
        $this->setUid()->setName($name)->setSlug($slug);
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Partenaire
     */
    public function setId(int $id): Partenaire
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Partenaire
     */
    public function setName(string $name): Partenaire
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Partenaire
     */
    public function setSlug(string $slug): Partenaire
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Partenaire
     */
    public function setContent(string $content): Partenaire
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Partenaire
     */
    public function setImage($image): Partenaire
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Partenaire
     */
    public function setUrl(string $url): Partenaire
    {
        $this->url = $url;
        return $this;
    }

    public function getExtrait(int $len = 150): string
    {
        return substr(strip_tags($this->getContent()), 0, $len);
    }

    /**
     * @return Event[]|ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param Event[]|ArrayCollection $events
     * @return Partenaire
     */
    public function setEvents($events)
    {
        $this->events = $events;
        return $this;
    }

}
