<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 12:16
 */

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\{HttpFoundation\File\UploadedFile,
    Security\Core\User\EquatableInterface,
    Security\Core\User\UserInterface,
    Validator\Constraints as Assert};


/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email déja enregistré")
 * @UniqueEntity(fields="phone", message="Numéro de téléphone déja enregistré")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends Entity implements UserInterface, EquatableInterface {

    const ROLES = ["ROLE" => " ", 'ROLE_EDITEUR' => 'ROLE_EDITEUR', 'ROLE_ADMIN' => "ROLE_ADMIN"];

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=191, unique=true)
     * @Assert\NotBlank(message="Renseignez une adresse email")
     * @Assert\Email(message="Renseignez une adresse email valide")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=191)
     * @Assert\NotBlank(message="Renseignez votre prénom")
     */
    private $name;

    /**
     * @ORM\Column(type="string",length=191)
     */
    private $phone;

    /**
     * @ORM\Column(type="string",length=191)
     * @Assert\NotBlank(message="Le champ adresse ne doit pas être vide")
     */
    private $location;

    /**
     * @ORM\Column(type="string",length=191)
     */
    private $photo = "no-image.png";

    /**
     * @var Event[] | ArrayCollection
     * @ORM\OneToMany(targetEntity="Event", mappedBy="user")
     */
    private $events;

    /**
     * @var Article[] | ArrayCollection
     * @ORM\OneToMany(targetEntity="Article", mappedBy="user")
     */
    private $articles;

    /**
     * @var Video[] | ArrayCollection
     * @ORM\OneToMany(targetEntity="Video", mappedBy="user")
     */
    private $videos;

    /**
     * @var \DateTime
     * @ORM\Column(name="lastlogin", type="datetime",nullable=true)
     */
    private $lastlogin;

    /**
     * @ORM\Column(type="string", length=191 , nullable=true)
     */
    private $confirmation_token;

    /**
     * @ORM\Column(type="string")
     */
    private $password;
    /**
     * @var string
     */
    private $plainPassword;

    public function __construct(string $name = "", string $email = "") {
        $this->setName($name)->setEmail($email)->setUid();
    }

    /**
     * @return int
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getRoles(): array {
        return $this->roles;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string {
        return $this->password;
    }

    public function getSalt() {
    }

    /**
     * @return string
     */
    public function getUsername(): ?string {
        return $this->email;
    }

    public function eraseCredentials() {
        $this->plainPassword = null;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string {
        return $this->email;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    /**
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles(array $roles): self {
        if (!in_array('ROLE_USER', $roles)) {
            $roles[] = 'ROLE_USER';
        }
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): ?string {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     *
     * @return $this
     */
    public function setPlainPassword(string $plainPassword): self {
        $this->plainPassword = $plainPassword;
        // forces the object to look "dirty" to Doctrine. Avoids
        // Doctrine *not* saving this entity, if only plainPassword changes
        $this->password = null;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param \DateTime $lastlogin
     *
     * @return $this
     */
    public function setLastlogin(\DateTime $lastlogin) {
        $this->lastlogin = $lastlogin;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastlogin(): ?\DateTime {
        return $this->lastlogin;
    }

    /**
     * @param string $confirmationToken
     *
     * @return $this
     */
    public function setConfirmationToken(string $confirmationToken): self {

        $this->confirmation_token = $confirmationToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmationToken(): ?string {
        return $this->confirmation_token;
    }

    /**
     * @return mixed
     */
    public function getPhone(): ?string {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return User
     */
    public function setPhone(string $phone): self {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocation(): ?string {
        return $this->location;
    }

    /**
     * @param mixed $location
     * @return User
     */
    public function setLocation(string $location): self {
        $this->location = $location;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     * @return User
     */
    public function setPhoto(?string $photo): self {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @param UserInterface $user
     *
     * @return bool
     */
    public function isEqualTo(UserInterface $user): ?bool {
        return $this->id == $user->getId();
    }

    public function hasRole($role) {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * @return Article[]|ArrayCollection
     */
    public function getArticles() {
        return $this->articles;
    }

    /**
     * @param Article[]|ArrayCollection $articles
     * @return User
     */
    public function setArticles($articles) {
        $this->articles = $articles;
        return $this;
    }

    /**
     * @return Video[]|ArrayCollection
     */
    public function getVideos() {
        return $this->videos;
    }

    /**
     * @param Video[]|ArrayCollection $videos
     * @return User
     */
    public function setVideos($videos) {
        $this->videos = $videos;
        return $this;
    }

    public function __toString(): ?string {
        return $this->name;
    }

    /**
     * @return Event[]|ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param Event[]|ArrayCollection $events
     * @return User
     */
    public function setEvents($events)
    {
        $this->events = $events;
        return $this;
    }

}