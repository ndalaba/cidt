<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/12/19
 * Time: 7:25 PM
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Section
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="sections")
 */
class Section extends Entity {

    const SEC_PER_PAGE = 5;

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $slug;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var ArrayCollection | Page[]
     * @ORM\OneToMany(targetEntity="Page", mappedBy="section")
     */
    private $pages;

    public function __construct(string $title = "", string $slug = "") {
        $this->setUid()->setTitle($title)->setSlug($slug);
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Section
     */
    public function setId(int $id): Section {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Section
     */
    public function setTitle(string $title): Section {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Section
     */
    public function setSlug(string $slug): Section {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Section
     */
    public function setDescription(string $description): Section {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Page[]|ArrayCollection
     */
    public function getPages() {
        return $this->pages;
    }

    /**
     * @param Page[]|ArrayCollection $pages
     * @return Section
     */
    public function setPages($pages) {
        $this->pages = $pages;
        return $this;
    }

    public function __toString(): string {
        return $this->title;
    }

}