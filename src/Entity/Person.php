<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class Person
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="persons")
 */
class Person extends Entity {

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     * @Assert\NotBlank(message="Le champ nom ne doit pas être vide")
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $slug;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     * @Assert\Image(mimeTypes = {"image/jpeg", "image/gif", "image/png"}, mimeTypesMessage="Les formats d'images acceptés sont jpg, png, gif")
     */
    private $image = 'noimage.png';
    /**
     * @var string
     * @ORM\Column(name="fonction", type="string", length=191)
     */
    private $fonction;

    public function __construct(string $name = "", string $slug = "") {
        $this->setUid()->setName($name)->setSlug($slug);
    }

    /**
     * @return int
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Person
     */
    public function setId(int $id): Person {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Person
     */
    public function setName(string $name): Person {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Person
     */
    public function setSlug(string $slug): Person {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Person
     */
    public function setContent(string $content): Person {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Person
     */
    public function setImage($image): Person {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getFonction(): ?string {
        return $this->fonction;
    }

    /**
     * @param string $fonction
     * @return Person
     */
    public function setFonction(string $fonction): Person {
        $this->fonction = $fonction;
        return $this;
    }

    public function getExtrait(int $len = 150): string {
        return substr(strip_tags($this->getContent()), 0, $len);
    }
}
