<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/12/19
 * Time: 7:25 PM
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class Category
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="articles")
 */
class Article extends Entity {

    const POSTS_PER_PAGE = 14;

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $top;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $slug;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     * @Assert\Image(mimeTypes = {"image/jpeg", "image/gif", "image/png"}, mimeTypesMessage="Les formats d'images acceptés sont jpg, png, gif")
     */
    private $image='noimage.png';

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="articles")
     */
    private $user;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="articles")
     */
    private $category;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $vue=0;

    public function __construct(string $title = "", string $slug = "") {
        $this->setUid()->setTitle($title)->setSlug($slug)->setImage("noimage.png");

    }

    public function isAuthor(User $user) {
        return $this->getUser() === $user;
    }

    public function getExtrait(int $len = 150): string {
        return substr(strip_tags($this->getContent()), 0, $len);
    }

    /**
     * @return int
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Article
     */
    public function setId(int $id): Article {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Article
     */
    public function setTitle(string $title): Article {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Article
     */
    public function setSlug(string $slug): Article {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Article
     */
    public function setContent(string $content): Article {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Article
     */
    public function setImage(?string $image): Article {
        $this->image = $image;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): ?User {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Article
     */
    public function setUser(User $user): Article {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory(): ?Category {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Article
     */
    public function setCategory(Category $category): Article {
        $this->category = $category;
        return $this;
    }

    /**
     * @return int
     */
    public function getVue(): ?int {
        return $this->vue;
    }

    /**
     * @param int $vue
     * @return Article
     */
    public function setVue(int $vue): Article {
        $this->vue = $vue;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTop(): ?bool {
        return $this->top;
    }

    /**
     * @param bool $top
     * @return Article
     */
    public function setTop(bool $top): Article {
        $this->top = $top;
        return $this;
    }

    public function __toString() {
        return $this->title;
    }
}