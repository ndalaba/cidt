<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 14:17
 */

namespace App\Security;


use App\Repository\UserRepository;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\{RedirectResponse, Request, Session\Session};
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\{Authentication\Token\TokenInterface,
    Encoder\UserPasswordEncoderInterface,
    Exception\AuthenticationException,
    Security,
    User\UserInterface,
    User\UserProviderInterface};
use Symfony\Component\Security\Guard\{AbstractGuardAuthenticator};
use App\Form\LoginFormType;
use Symfony\Component\Security\Http\Util\TargetPathTrait;


class LoginFormAuthenticator extends AbstractGuardAuthenticator {

    private $formFactory, $userRepository, $router, $passwordEncoder, $remember, $session;

    public function __construct(FormFactoryInterface $formFactory, UserRepository $userRepository, RouterInterface $router, UserPasswordEncoderInterface $encoder) {
        $this->formFactory = $formFactory;
        $this->userRepository = $userRepository;
        $this->router = $router;
        $this->passwordEncoder = $encoder;
        $this->session = new Session();

    }

    public function supports(Request $request) {
        return $request->getPathInfo() == '/login' && $request->isMethod('POST');
    }

    public function start(Request $request, AuthenticationException $authException = null) {
        $url = $this->router->generate('login');

        return new RedirectResponse($url);
    }

    public function getCredentials(Request $request) {
        $form = $this->formFactory->create(LoginFormType::class);
        $form->handleRequest($request);
        $data = $form->getData();
        $this->remember = $request->request->get('_remember_me');

        $request->getSession()->set(Security::LAST_USERNAME, $data['_username']);

        return $data;
    }

    public function getUser($credentials, UserProviderInterface $userProvider) {
        $username = $credentials['_username'];
        $user = $this->userRepository->findOneBy(['email' => $username]);
        if (!$user) {
            $this->session->getFlashBag()->set('error', "Votre e-mail ou votre mot de passe est invalide. Réessayez.");
        }

        return $user;
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function checkCredentials($credentials, UserInterface $user) {
        $password = $credentials['_password'];
        if ($this->passwordEncoder->isPasswordValid($user, $password)) {
            $user->setLastLogin(new \DateTime());
            $this->userRepository->save($user);
            return true;
        }
        $this->session->getFlashBag()->set('error', "Votre e-mail ou votre mot de passe est invalide. Réessayez.");

        return false;
    }

    use TargetPathTrait;

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey) {
        $uri = $request->getSession()->get('referer');
        $targetPath = $request->getSession()->get('_security.main.target_path');
        if (isset($uri)) {
            return new RedirectResponse($uri);
        } else {
            if (isset($targetPath)) {
                return new RedirectResponse($targetPath);
            }
        }

        return new RedirectResponse($this->router->generate('admin_home'));
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

        return new RedirectResponse($this->router->generate('login'));
    }

    public function supportsRememberMe() {
        return $this->remember != null;
    }
}