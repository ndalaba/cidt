<?php
/**
 * Created by PhpStorm.
 * Member: dmn
 * Date: 3/14/19
 * Time: 1:34 PM
 */

namespace App\Repository;


use App\Entity\Member;
use App\Utils\Pagination;
use Doctrine\Common\Persistence\ManagerRegistry;

class MemberRepository extends EntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Member::class);
    }

    public function devalidated()
    {
        return $this->findBy(['validated' => false]);
    }

    /**
     * @param $email
     *
     * @return Member
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function byEmail($email): Member
    {
        return $this->createQueryBuilder("u")->where('u.email=:email')->setParameter("email", $email)->getQuery()->getSingleResult();
    }

    public function activate(array $uids)
    {
        if (!count($uids) or empty($uids)) return;
        $sql = "UPDATE " . $this->_entityName . " Q SET Q.validated=1 WHERE Q.uid IN(:uids) ";
        $query = $this->_em->createQuery($sql)->setParameter('uids', $uids);
        $query->execute();
    }

    public function deActivate(array $uids)
    {
        if (!count($uids) or empty($uids)) return;
        $sql = "UPDATE " . $this->_entityName . " Q SET Q.validated=0 WHERE Q.uid IN(:uids) ";
        $query = $this->_em->createQuery($sql)->setParameter('uids', $uids);
        $query->execute();
    }

    public function filter(array $critera, int $start = 0, $end = Pagination::PER_PAGE)
    {
        if (!count($critera)) return $this->last();
        $query = $this->createQueryBuilder('a');
        foreach ($critera as $key => $value) {
            is_string($value) ? $query->where('a.' . $key . " LIKE :value")->setParameter('value', '%' . $value . '%') : $query->where('a.' . $key . '=:value')->setParameter('value', $value);
        }
        return $query->orderBy('a.createdAt', 'DESC')->setFirstResult($start)->setMaxResults($end)->getQuery()->getResult();
    }

}