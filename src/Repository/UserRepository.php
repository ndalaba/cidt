<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 12:18
 */

namespace App\Repository;

use App\Entity\User;
use App\Utils\Pagination;
use Doctrine\Common\Persistence\ManagerRegistry;

class UserRepository extends EntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, User::class);
    }

    /**
     * @param $email
     *
     * @return User
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function byEmail($email): User {
        return $this->createQueryBuilder("u")->where('u.email=:email')->setParameter("email", $email)->getQuery()->getSingleResult();
    }
    public function activate(array $uids) {
        if (!count($uids) or empty($uids)) return;
        $sql = "UPDATE " . $this->_entityName . " Q SET Q.activated=1 WHERE Q.uid IN(:uids) ";
        $query = $this->_em->createQuery($sql)->setParameter('uids', $uids);
        $query->execute();
    }
    public function deActivate(array $uids) {
        if (!count($uids) or empty($uids)) return;
        $sql = "UPDATE " . $this->_entityName . " Q SET Q.activated=0 WHERE Q.uid IN(:uids) ";
        $query = $this->_em->createQuery($sql)->setParameter('uids', $uids);
        $query->execute();
    }
    public function filter(array $critera, int $start = 0, $end = Pagination::PER_PAGE) {
        if (!count($critera)) return $this->last();
        $query = $this->createQueryBuilder('a');
        foreach ($critera as $key => $value) {
            is_string($value) ? $query->where('a.' . $key . " LIKE :value")->setParameter('value', '%' . $value . '%') : $query->where('a.' . $key . '=:value')->setParameter('value', $value);
        }
        return $query->orderBy('a.createdAt', 'DESC')->setFirstResult($start)->setMaxResults($end)->getQuery()->getResult();

    }

}