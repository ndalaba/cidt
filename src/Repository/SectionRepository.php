<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 1:34 PM
 */

namespace App\Repository;


use App\Entity\Section;
use Doctrine\Common\Persistence\ManagerRegistry;

class SectionRepository extends EntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Section::class);
    }
}