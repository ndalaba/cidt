<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 1:34 PM
 */

namespace App\Repository;


use App\Entity\Partenaire;
use Doctrine\Common\Persistence\ManagerRegistry;

class PartenaireRepository extends EntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Partenaire::class);
    }
}