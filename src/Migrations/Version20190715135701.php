<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190715135701 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Events (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, title VARCHAR(191) NOT NULL, slug VARCHAR(191) NOT NULL, content LONGTEXT NOT NULL, begin_at DATETIME NOT NULL, end_at DATETIME NOT NULL, location VARCHAR(191) NOT NULL, phone VARCHAR(191) NOT NULL, email VARCHAR(191) NOT NULL, vue INT NOT NULL, image VARCHAR(191) NOT NULL, uid VARCHAR(191) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, published TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_542B527C539B0606 (uid), INDEX IDX_542B527CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_partenaire (event_id INT NOT NULL, partenaire_id INT NOT NULL, INDEX IDX_6A117A5C71F7E88B (event_id), INDEX IDX_6A117A5C98DE13AC (partenaire_id), PRIMARY KEY(event_id, partenaire_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE partenaires (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(191) NOT NULL, slug VARCHAR(191) NOT NULL, content LONGTEXT NOT NULL, image VARCHAR(191) NOT NULL, url VARCHAR(191) NOT NULL, uid VARCHAR(191) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, published TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_D230102E539B0606 (uid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE partenaire_event (partenaire_id INT NOT NULL, event_id INT NOT NULL, INDEX IDX_F24D4B1D98DE13AC (partenaire_id), INDEX IDX_F24D4B1D71F7E88B (event_id), PRIMARY KEY(partenaire_id, event_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Events ADD CONSTRAINT FK_542B527CA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE event_partenaire ADD CONSTRAINT FK_6A117A5C71F7E88B FOREIGN KEY (event_id) REFERENCES Events (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_partenaire ADD CONSTRAINT FK_6A117A5C98DE13AC FOREIGN KEY (partenaire_id) REFERENCES partenaires (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE partenaire_event ADD CONSTRAINT FK_F24D4B1D98DE13AC FOREIGN KEY (partenaire_id) REFERENCES partenaires (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE partenaire_event ADD CONSTRAINT FK_F24D4B1D71F7E88B FOREIGN KEY (event_id) REFERENCES Events (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users CHANGE lastlogin lastlogin DATETIME DEFAULT NULL, CHANGE confirmation_token confirmation_token VARCHAR(191) DEFAULT NULL');
        $this->addSql('ALTER TABLE articles CHANGE user_id user_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE videos CHANGE user_id user_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event_partenaire DROP FOREIGN KEY FK_6A117A5C71F7E88B');
        $this->addSql('ALTER TABLE partenaire_event DROP FOREIGN KEY FK_F24D4B1D71F7E88B');
        $this->addSql('ALTER TABLE event_partenaire DROP FOREIGN KEY FK_6A117A5C98DE13AC');
        $this->addSql('ALTER TABLE partenaire_event DROP FOREIGN KEY FK_F24D4B1D98DE13AC');
        $this->addSql('DROP TABLE Events');
        $this->addSql('DROP TABLE event_partenaire');
        $this->addSql('DROP TABLE partenaires');
        $this->addSql('DROP TABLE partenaire_event');
        $this->addSql('ALTER TABLE articles CHANGE user_id user_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users CHANGE lastlogin lastlogin DATETIME DEFAULT \'NULL\', CHANGE confirmation_token confirmation_token VARCHAR(191) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE videos CHANGE user_id user_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL');
    }
}
