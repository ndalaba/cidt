<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190916140243 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Members (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(191) NOT NULL, phone VARCHAR(191) DEFAULT NULL, email VARCHAR(191) DEFAULT NULL, age INT DEFAULT NULL, country VARCHAR(191) DEFAULT NULL, adresse TINYTEXT DEFAULT NULL, validated TINYINT(1) NOT NULL, uid VARCHAR(191) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, published TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8A1DEB63539B0606 (uid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE articles CHANGE user_id user_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE Events CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE pages CHANGE section_id section_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users CHANGE lastlogin lastlogin DATETIME DEFAULT NULL, CHANGE confirmation_token confirmation_token VARCHAR(191) DEFAULT NULL');
        $this->addSql('ALTER TABLE videos CHANGE user_id user_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE Members');
        $this->addSql('ALTER TABLE Events CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE articles CHANGE user_id user_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE pages CHANGE section_id section_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users CHANGE lastlogin lastlogin DATETIME DEFAULT \'NULL\', CHANGE confirmation_token confirmation_token VARCHAR(191) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE videos CHANGE user_id user_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL');
    }
}
