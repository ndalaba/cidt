<?php
/**
 * Created by PhpStorm.
 * User: florent.fremont
 * Date: 27/02/2017
 * Time: 21:15
 */

namespace App\Utils;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader {
    private $uploadPath;

    /**
     * @return mixed
     */
    public function getUploadPath(): ?string
    {
        return $this->uploadPath;
    }

    /**
     * @param mixed $uploadPath
     * @return FileUploader
     */
    public function setUploadPath(String $uploadPath)
    {
        $this->uploadPath = $uploadPath;
        return $this;
    }

    /**
     * @param UploadedFile $file
     * @param string $targetDir
     * @return array
     */
    public function upload(UploadedFile $file, string $targetDir, string $name = "")
    {
        if ($name == "")
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
        else
            $fileName = $name . '.' . $file->guessExtension();
        if ($this->getUploadPath() && $targetDir) {
            $uploadDir = $this->getUploadPath() . $targetDir;
            $file->move($uploadDir, $fileName);
            return ['uploaded' => true, 'fileName' => $fileName];
        }
        return ['uploaded' => false, 'fileName' => ''];
    }
}