<?php
/**
 * Created by PhpStorm.
 * User: ndalaba
 * Date: 04/04/2017
 * Time: 18:28
 */

namespace App\Utils;


class EmailManager {

    private $mailer;

    public function __construct(\Swift_Mailer $mailer) {
        $this->mailer = $mailer;
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $bodyhtml
     * @param string $bodyText
     * @return int
     */
    public function sendMail($from, $to, $subject, $bodyhtml, $bodyText = null) {
        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody($bodyhtml, 'text/html');
        if ($bodyText !== null)
            $message->addPart($bodyText, 'text/plain');

        $statut = $this->mailer->send($message);
        return $statut;
    }
}