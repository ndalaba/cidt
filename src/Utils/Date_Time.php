<?php


namespace App\Utils;


class Date_Time {

    public function dateToLongFrench(\DateTime $date, string $format): string
    {
        $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
        $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
        return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, $date->getTimestamp())));
    }
    public function dateToShortFrench(\DateTime $date, string $format): string
    {
        $english_days = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
        $french_days = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');
        $english_months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        $french_months = array('jan', 'fév', 'mars', 'avr', 'mai', 'jui', 'juil', 'août', 'sep', 'oct', 'nov', 'déc');
        return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, $date->getTimestamp())));
    }
}