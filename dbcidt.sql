-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: dbcidt
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Events`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `begin_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vue` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_542B527C539B0606` (`uid`),
  KEY `IDX_542B527CA76ED395` (`user_id`),
  CONSTRAINT `FK_542B527CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Events`
--

LOCK TABLES `Events` WRITE;
/*!40000 ALTER TABLE `Events` DISABLE KEYS */;
INSERT INTO `Events` VALUES (1,1,'Grande Soirée AJRATF le 14 Avril à PARIS','grande-soiree-ajratf-le-14-avril-a-paris','<p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">L’Association des Jeunes Ressortissants et Amis de Télimélé en France (AJRATF) est heureuse de vous convier à une Grande Soirée Dansante qu’elle organise le:</p><p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\"><span style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: 700; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Vendredi 14 Avril 2017 à partir de 22h au SILY CLUB sis 4 Rue Louis Armand, 92600 Anières-Sur-Seine.</span></p><p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Cette soirée, organisée dans le cadre des activités culturelles et humanitaires de l’AJRATF connaitra la participation de la&nbsp;<span style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: 700; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Miss Guinée France 2017 accompagnée de ses dauphines.</span></p><p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Plusieurs associations Guinéennes d’Europe seront également au Rendez–vous :</p><p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\"><span style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: 700; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">La Coordination Internationale pour le Développement de Télimélé (CIDT)<br style=\"box-sizing: inherit;\">La Coordination des Associations Guinéennes de France (CAGF)<br style=\"box-sizing: inherit;\">L’Association des Jeunes Guinéens de France (AJGF)<br style=\"box-sizing: inherit;\">L’Association des Ressortissants de Tougué (ART)<br style=\"box-sizing: inherit;\">L’Association des Ressortissants du Massif de Tangué (Mali)<br style=\"box-sizing: inherit;\">Les Ressortissants de la Belgique, Suisse, Hollande, Italie, Espagne.</span></p><p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Venez nombreux profiter d’une soirée placée sous le signe de la détente et des retrouvailles chaleureuses entre amis au rythme de différentes sonorités : Guinéennes, Zouk, Salsa, Mbalakh, Coupé décalé, Hip-hop, RNB…</p><p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Tarifs : – Prévente : 15 euros – Sur place : 20 euros avec conso<br style=\"box-sizing: inherit;\">ACCES:<br style=\"box-sizing: inherit;\">METRO LIGNE 13 GABRIEL PERI, puis BUS 177 Arrêt LAURENT CELY<br style=\"box-sizing: inherit;\">RER C STATION LES GRESILLON</p>','2019-08-14 13:00:00','2019-08-14 23:59:00','4 Rue Louis Armand, 92600 Anières-Sur-Seine.','07 77 49 24 80','bureau-ajratf@googlegroups.com',1,'3cdcb1879b23f38cdafb79551c5ca7c7.jpeg','5d2f1e37d02cf','2019-07-17 13:10:16','2019-07-19 00:51:58',1),(2,1,'Grande Soirée AJRATF Vendrédi 24 Juillet 2015','grande-soiree-ajratf-vendredi-24-juillet-2015','<div style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Pour fêter l’été en beauté, l’Association des Jeunes Ressortissants et Amis de Télimélé en France (AJRATF) est heureuse de vous convier à une grande soirée dansante qu’elle organise le:</div><div style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\"><span style=\"box-sizing: inherit; font-weight: 700;\">Vendredi 24 Juillet 20<span class=\"il\" style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">1</span>5</span>&nbsp;à partir de 23h au Silly Club au&nbsp;<span style=\"box-sizing: inherit; font-weight: 700;\">45 Rue de la commune de Paris, 93230 ROMAINVILLE</span></div><div style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Cette soirée, organisée dans le cadre des activités culturelles et humanitaires de l’AJRATF connaitra la participation de la&nbsp;<span style=\"box-sizing: inherit; font-weight: 700;\">Miss Guinée France 20<span class=\"il\" style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">1</span>5 accompagnée de ses dauphines</span>.</div><div style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Plusieurs associations Guinéennes d’Europe seront au Rendez –vous&nbsp;:</div><div style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\"><span style=\"box-sizing: inherit; font-weight: 700;\"><i style=\"box-sizing: inherit;\">Association des Jeunes Guinéens de France</i></span></div><div style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\"><span style=\"box-sizing: inherit; font-weight: 700;\"><i style=\"box-sizing: inherit;\">Le club de Réflexion CADEV-GUINEE</i></span></div><div style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\"><span style=\"box-sizing: inherit; font-weight: 700;\"><i style=\"box-sizing: inherit;\">Les&nbsp; Amis de la 4<span class=\"il\" style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">1</span><span style=\"box-sizing: inherit; border: 0px; font-size: 9.75px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 0; position: relative; top: -0.5em;\">ème</span>&nbsp;Promotion FDSEG de l’Université GAMAL ABDEL NASSER</i></span></div><div style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\"><span style=\"box-sizing: inherit; font-weight: 700;\"><i style=\"box-sizing: inherit;\">Les Ressortissants de la Belgique, Suisse, Hollande, Italie, Espagne.</i></span></div><div style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Venez nombreux profiter d’une soirée placée sous le signe de la détente et des retrouvailles chaleureuses entre amis au rythme de différentes sonorités : Guinéennes, Zouk, Salsa, Mbalakh, Coupé décalé, Hip-hop, RNB…</div><div style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">&nbsp;&nbsp;<span style=\"box-sizing: inherit; font-weight: 700;\"><u style=\"box-sizing: inherit;\">Tarifs :</u></span><br style=\"box-sizing: inherit;\">–<span style=\"box-sizing: inherit; font-weight: 700;\">&nbsp;Prévente :&nbsp;<span class=\"il\" style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">1</span>5 euros avec conso<br style=\"box-sizing: inherit;\">– Sur place : 20 euros avec conso<br style=\"box-sizing: inherit;\">ACCES: METRO LIGNE 5 – Bobigny Raymond Queneau<br style=\"box-sizing: inherit;\">INFO LINE: 06 23 5<span class=\"il\" style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">1</span>&nbsp;66 28 / 06 05 8<span class=\"il\" style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">1</span>&nbsp;42 74 / 06 42 00 0<span class=\"il\" style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">1</span>&nbsp;42/06 6<span class=\"il\" style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">1</span>&nbsp;27 07 96</span></div>','2015-07-25 23:00:00','2015-07-26 05:00:00','45 Rue de la commune de Paris, 93230 ROMAINVILLE','06 23 51 66 28','bureau-ajratf@googlegroups.com',3,'1b56139a6678c991acdd509d7a3db6ae.jpeg','5d302ef19af8e','2019-07-18 08:33:54','2019-07-19 00:50:50',1);
/*!40000 ALTER TABLE `Events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `top` tinyint(1) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vue` int(11) NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BFDD3168539B0606` (`uid`),
  KEY `IDX_BFDD3168A76ED395` (`user_id`),
  KEY `IDX_BFDD316812469DE2` (`category_id`),
  CONSTRAINT `FK_BFDD316812469DE2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `FK_BFDD3168A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (8,1,3,0,'Signature de la convention pour le projet de Sinta','signature-de-la-convention-pour-le-projet-de-sinta','<p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Le bureau a le plaisir de vous informer qu’il a signé la convention avec le FORIM le Mercredi 08 Octobre 2014 pour le décaissement de la première tranche (80%) du montant de la subvention accordée de 15 000 euros.<br style=\"box-sizing: inherit;\">Le bureau convoquera une réunion particulière pour commencer les préparatifs afin que les travaux de rénovation puisse démarrer dans les brefs délais.</p><p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Le bureau vous tiendra au courant pour la suite.</p>','7e9905340b58be68c70454cf8b6e8a0f.jpeg',1,'5d30272506694','2019-07-18 08:00:38','2019-07-19 00:03:02',1),(9,1,3,0,'Dernière Info sur Ebola à Télimélé','derniere-info-sur-ebola-a-telimele','<p><span style=\"color: rgb(76, 76, 76); font-family: Quicksand, sans-serif; font-size: 13px;\">Vous trouverez en pièce jointe un très bon document de l’OMS sur quelques détails du virus Ebola à Télimélé. La situation se maitrise peu à peu.</span><br></p>','cb0a5c1ea687541fd692036d718024e4.jpeg',1,'5d3027550e9c8','2019-07-18 08:01:26','2019-07-19 00:03:05',1),(10,1,3,0,'Obtention de financement pour la renovation du centre de santé de Sinta','obtention-de-financement-pour-la-renovation-du-centre-de-sante-de-sinta','<div style=\"box-sizing: inherit; border: 0px;  margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Suite à la demande que nous avons déposé au mois de Mai, j’ai le plaisir de vous informer que notre projet «&nbsp;Rénovation et équipement du centre de santé de sinta&nbsp;» a eu un avis favorable du FORIM pour un montant global de&nbsp;<span class=\"il\" style=\"box-sizing: inherit; border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">1</span>5 000 euros.</div><div style=\"box-sizing: inherit; border: 0px;  margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \"></div><div style=\"box-sizing: inherit; border: 0px;  margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Nous reviendrons vers vous pour plus de détails.</div><div style=\"box-sizing: inherit; border: 0px;  margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \"></div><div style=\"box-sizing: inherit; border: 0px;  margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Bien cordialement</div><div style=\"box-sizing: inherit; border: 0px;  margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \"></div><div style=\"box-sizing: inherit; border: 0px;  margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Ibrahima Sory DIALLO</div><div style=\"box-sizing: inherit; border: 0px;  margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Trésorier AJRATF</div>','496e348459e4ad05de3c64bc6aa7e453.jpeg',12,'5d3027729b0ff','2019-07-18 08:01:55','2019-07-19 01:07:20',1),(11,1,1,0,'Envoi de livres et d’ordinateurs','envoi-de-livres-et-dordinateurs','<p><span style=\"color: rgb(76, 76, 76); font-family: Quicksand, sans-serif; font-size: 13px;\">Envoi de livres et d’ordinateurs</span><br style=\"box-sizing: inherit; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif; font-size: 13px;\"><span style=\"color: rgb(76, 76, 76); font-family: Quicksand, sans-serif; font-size: 13px;\">Valeur estimée 1500 euros</span><br></p>','6f686365d3661bc3d7125bc1600c6006.jpeg',0,'5d302983206ea','2019-07-18 08:10:44','2019-07-18 08:10:44',1),(12,1,1,0,'Rénovation du lycée Ley-Wendou','renovation-du-lycee-ley-wendou','<p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Rénovation du lycée Ley-Wendou</p><p style=\"box-sizing: inherit; border: 0px; font-size: 13px; margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif;\">Budget global du projet : 18.000 euros<br style=\"box-sizing: inherit;\">Requête au PRAOSIM: 12.600 euros</p>','5df4b6caa9c4ef2c1ff8ee8603a98a87.jpeg',1,'5d3029ac1af9e','2019-07-18 08:11:25','2019-07-19 00:03:08',1),(13,1,1,0,'Envoi de vetements pour les enseignants','envoi-de-vetements-pour-les-enseignants','<p><span style=\"color: rgb(76, 76, 76); font-family: Quicksand, sans-serif; font-size: 13px;\">Envoi de vetements pour les enseignants</span><br style=\"box-sizing: inherit; color: rgb(76, 76, 76); font-family: Quicksand, sans-serif; font-size: 13px;\"><span style=\"color: rgb(76, 76, 76); font-family: Quicksand, sans-serif; font-size: 13px;\">Valeur estimée : 500 euros</span><br></p>','c40b469d4b2eaf18dfe38a601da4ae8c.jpeg',5,'5d3029d1eb5ac','2019-07-18 08:12:03','2019-07-19 00:46:18',1);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_3AF34668539B0606` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Projets','projets','Projets réalisés ou en cours de réalisations de l\'AJRATF','5ca1c9d207fef','2019-04-01 10:20:34','2019-07-17 11:43:39',0),(3,'News','news','Informations relatives à l\'Armée de mer Guinéenne','5ca1ca0e11c24','2019-04-01 10:21:34','2019-04-01 10:21:34',0);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_partenaire`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_partenaire` (
  `event_id` int(11) NOT NULL,
  `partenaire_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`,`partenaire_id`),
  KEY `IDX_6A117A5C71F7E88B` (`event_id`),
  KEY `IDX_6A117A5C98DE13AC` (`partenaire_id`),
  CONSTRAINT `FK_6A117A5C71F7E88B` FOREIGN KEY (`event_id`) REFERENCES `Events` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_6A117A5C98DE13AC` FOREIGN KEY (`partenaire_id`) REFERENCES `partenaires` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_partenaire`
--

LOCK TABLES `event_partenaire` WRITE;
/*!40000 ALTER TABLE `event_partenaire` DISABLE KEYS */;
INSERT INTO `event_partenaire` VALUES (1,2);
/*!40000 ALTER TABLE `event_partenaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20190715135701','2019-07-15 13:57:17'),('20190715144209','2019-07-15 14:42:14'),('20190715144242','2019-07-15 14:42:46'),('20190731114735','2019-07-31 11:47:50');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2074E575539B0606` (`uid`),
  KEY `IDX_2074E575D823E37A` (`section_id`),
  CONSTRAINT `FK_2074E575D823E37A` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'Le mot du Président','le-mot-du-president','<p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Chers lecteurs, vous êtes ici sur un espace dédié aux actions volontaristes à destination d’une des plus belles préfecture de la Guinée. Le site de l’Association des Jeunes Ressortissants et Amis de Télémilé en France représente une des rares vitrines de cette partie du monde.</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Sur l’identité de Télémilé, vous apprendrez son histoire, ses coutumes et religions, son économie… Mais aussi toutes les actions menées au bénéfice des populations locales par les membres de cette structure.</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Notre objectif consiste principalement à donner une nouvelle impulsion économique à Télémilé à travers des projets entre autres : éducatif, culturel, alimentaire, sanitaire…</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">La force de notre association réside dans sa cohésion, son expertise, sa générosité et son esprit d’équipe. C’est grâce à ces atouts que l’AJRATF est perçue par nos interlocuteurs institutionnels comme une association de qualité, qui milite sérieusement pour le développement du SUD, comme en témoigne les multiples projets déjà financés par le PRAOSIM.</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Mes priorités sont les suivantes :</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">1. Une plus grande interaction entre les membres de l’AJRATF</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">2. Booster davantage les nouvelles adhésions de ressortissant au sein de l’association</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">3. Donner une dimension nouvelle aux projets portés par l’association</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">4. Plus de visibilité dans le monde associatif des diasporas guinéennes voire africaines</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Dans cette optique, j’ai l’intention d’oeuvrer avec ferveur avec l’appui de tous les membres et sympathisants de l’AJRATF.</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Pour finir, je tiens à rendre hommage à notre Président sortant Mamadou Sita BAH pour son action à la tête de l’association. Il a su poursuivre, avec tout le talent et la convivialité qui le caractérise, le combat engagé par nos prédécesseurs pour le développement et le rayonnement de L’AJRATF et de Télémilé.</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Toujours réinventer l’association !</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Merci.</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Mamadou Seydou DIALLO<br style=\"box-sizing: inherit;\">Président de l’AJRATF</p>','87f3e72a73fd662097edc5bd0a5a2940.jpeg','5ca1dfa7afb85','2019-04-01 11:53:43','2019-07-31 11:51:26',1,1),(3,'LA CIDT','la-cidt','<p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">L’Association des Jeunes Ressortissants et Amis de Télémélé en France (AJRATF) a été créée en 2008 dans le but de raffermir les liens entre les ressortissants de Télémélé d’une part et d’autre part de disposer d’une structure à travers laquelle on pourrait apporter des solutions à certains problèmes rencontrés par notre ville. En effet car jusqu’à présent et pour diverses raisons, les jeunes étaient souvent éloignés des préoccupations de développement de la ville. Elle ne prétend en aucun cas se substituer aux autres associations de développement de la préfecture. Au contraire, l’AJRATF constitue un complément dont le but est d’attirer plus de jeunes, qui force est de le répéter constitue l’avenir de la Guinée.</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Télémélé est l’une des préfectures la moins lotie de la Guinée en dépit de sa proximité géographique avec la capitale et de ses fortes potentialités en ressources humaines. La préfecture n’a jamais suscité de véritable engouement de la part de ses ressortissants pour son développement , préjuge–t-on; cependant, ses ressortissants furent parmi les premiers à se constituer en association en France. Nous saluons de ce fait leur dévouement. Certes, des efforts ont été entrepris, mais le rassemblement de toutes les couches originaires de la région serait important pour pouvoir atteindre de résultats plus probants. C’est pourquoi, cette association fut créée en France pour regrouper des jeunes de diverses catégories socioprofessionnelles et de divers horizons (aussi bien immigrés qu’issus de l’immigration). Nous comptons prochainement nous mettre en rapport avec les associations des autres villes européennes afin de partager nos expériences, créer des synergies et rationaliser nos projets de terrain.</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Les statuts et le règlement intérieur de l’AJRATF sont consultables sur le site internet de l’association. Chaque année, l’Assemblée Générale définit des activités afin de créer des liens de fraternité entre les membres et sympathisants de l’association et, trouver des ressources pour financer ses projets envers la préfecture car pour nous :</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">« Il faut que les jeunes s’engagent dans la vie de la collectivité. Il ne faudrait pas qu’ils hésitent à bousculer leurs ainés, tout en bénéficiant des expériences de ces derniers »</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 1.6em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Nous invitons les uns et les autres à se joindre à nos activités pour le développement de Télémélé. Nous sommes restés pendant longtemps à la marge de développement infrastructurel par rapport à certaines préfectures de la Guinée. Une prise de conscience profonde est donc nécessaire pour sortir de la stagnation et de l’isolement. Nous restons convaincus que la meilleure façon de participer au développement de son pays est de s’impliquer à la base par le biais, entre autres des mouvements associatifs car, la réussite du global passe par une implication du local.</p><p style=\"box-sizing: inherit; border: 0px;  margin-bottom: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(76, 76, 76); \">Avec nos ressources – constituées principalement de cotisations des membres et des activités culturelles – nos comptons financer périodiquement des projets d’assistance dans l’enseignement, la scolarisation des filles et, des projets à long terme (installation de centres de formation en informatique, rénovation d’établissements scolaires etc.). Pour réaliser nos desseins, nous comptons sur votre aide. Pour cela, devenez membre ou sympathisant de l’association. En outre, pour tout don, legs ou suggestions, prière de nous contacter</p>','304f86d3e73ea9ee007c2faca188d7c7.jpeg','5d2cb3c11984e','2019-07-15 17:11:29','2019-07-31 11:51:56',1,1),(4,'Santé','sante','LA CIDT s’engage aux côtés des autorités sanitaires et des organisations de la société civile pour améliorer la santé .','cdd4fe397bf025312366c69391d8337d.png','5d419191e5c14','2019-07-31 13:03:14','2019-07-31 13:03:14',1,2),(5,'Socio-Economique','socio-economique','Initier et aider à développer des actions concrètes pour améliorer la qualité de vie des habitants de Télémélé','0c36a7dcfdadab7491dd6bc636687df7.png','5d4191c925f98','2019-07-31 13:04:09','2019-07-31 13:04:09',1,2),(6,'Education','education','Favoriser l’accès de tous à l’éducation et améliorer les systèmes éducatifs, pour qu’ils répondent aux besoins de chacun.','45c97f2d65aea87d823a114bc46df040.png','5d4191e710d1b','2019-07-31 13:04:39','2019-07-31 13:04:39',1,2);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partenaires`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partenaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D230102E539B0606` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partenaires`
--

LOCK TABLES `partenaires` WRITE;
/*!40000 ALTER TABLE `partenaires` DISABLE KEYS */;
INSERT INTO `partenaires` VALUES (1,'Partenaire 1','partenaire-1','Partenaire 1','9c6055d1ba1e15a3f44c5381574cd0f8.png','#','5d2c87fad6137','2019-07-15 14:04:43','2019-07-15 14:04:43',1),(2,'Partenaire 2','partenaire-2','Partenaire 2','0daff13a87ed92ed6e8253a6a5eed470.png','#','5d2c880a43d32','2019-07-15 14:04:58','2019-07-15 14:04:58',1),(3,'Partenaire 3','partenaire-3','Partenaire 3','912efe208fcb864b7b0386d4e5c8769e.png','#','5d2c881918db2','2019-07-15 14:05:13','2019-07-15 14:05:13',1),(4,'Partenaire 4','partenaire-4','Partenaire 4','6e6da1a25cfb91b92846fc0036bedd53.png','#','5d2c882a035c2','2019-07-15 14:05:30','2019-07-15 14:05:30',1);
/*!40000 ALTER TABLE `partenaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persons`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fonction` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A25CC7D3539B0606` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persons`
--

LOCK TABLES `persons` WRITE;
/*!40000 ALTER TABLE `persons` DISABLE KEYS */;
INSERT INTO `persons` VALUES (1,'Souleymane','souleymane','L’Armée de Mer est commandée par un officier supérieur de marine appelé Chef d’Etat Major nommé par décret du président de la république ; il exerce son autorité sur l’Etat Major et  l’ensemble des forces navales  de l’Armée de Mer.','5d7a9be91262c31816bfc103f5ef119b.jpeg','Le Chef  d’Etat Major','5ca1ea5266ea6','2019-04-01 12:39:14','2019-04-01 12:39:14',1),(2,'Pépe Théa','p-pe-th-a','L’Armée de Mer est commandée par un officier supérieur de marine appelé Chef d’Etat Major nommé par décret du président de la république ; il exerce son autorité sur l’Etat Major et  l’ensemble des forces navales  de l’Armée de Mer.','3a25fa6229af25f8c932080fc0f768c8.jpeg','Chef d\'état major adjoint','5ca1ea76e1a78','2019-04-01 12:39:51','2019-04-01 12:39:51',1),(3,'Alpha Condé','alpha-cond-','L’Armée de Mer est commandée par un officier supérieur de marine appelé Chef d’Etat Major nommé par décret du président de la république ; il exerce son autorité sur l’Etat Major et  l’ensemble des forces navales  de l’Armée de Mer.','021a823667376bfac1b55db441abc57a.jpeg','Président','5ca1ea9c2123b','2019-04-01 12:40:28','2019-04-01 12:40:28',1),(4,'Papi Touré','papi-tour-','L’Armée de Mer est commandée par un officier supérieur de marine appelé Chef d’Etat Major nommé par décret du président de la république ; il exerce son autorité sur l’Etat Major et  l’ensemble des forces navales  de l’Armée de Mer.','e313ca55925bc23ab4f8c5597574124f.jpeg','Chef de brigade','5ca1eac96dda2','2019-04-01 12:41:13','2019-04-01 12:41:13',1);
/*!40000 ALTER TABLE `persons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2B964398539B0606` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,'CIDT','cidt','Coordination Internationale pour le Développement de Télimélé','5d4180615ce38','2019-07-31 11:49:53','2019-07-31 11:49:53',0),(2,'Activités','activites','Activités','5d4180766f2a6','2019-07-31 11:50:14','2019-07-31 11:50:14',0);
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `confirmation_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1483A5E9E7927C74` (`email`),
  UNIQUE KEY `UNIQ_1483A5E9539B0606` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'[\"ROLE_ADMIN\",\"ROLE_USER\"]','dmn@dev-hoster.com','Diallo','78976789','mon adresse','4e89d207a0ddadf297b4ea7e92f9b81f.png','2019-07-31 11:48:24',NULL,'$2y$13$7F4DV6mRf4bB2/dTpYbWGeoKK9MOe5zMjJmj5AgvrvLWX6LZxroau','5c9b7369cf90b','2019-03-27 13:58:18','2019-07-31 11:48:24',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `vue` int(11) NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_29AA6432539B0606` (`uid`),
  KEY `IDX_29AA6432A76ED395` (`user_id`),
  KEY `IDX_29AA643212469DE2` (`category_id`),
  CONSTRAINT `FK_29AA643212469DE2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `FK_29AA6432A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-12 11:16:54
